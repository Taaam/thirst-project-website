﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Please contact us at.</h3>
    <address>
        1234 Test st.<br />
       Long Beach, CA 90508<br />
        <abbr title="Phone">P:</abbr>
        123.456.7890
    </address>

</asp:Content>
