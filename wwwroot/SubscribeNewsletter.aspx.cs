﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SubscribeNewsletter : System.Web.UI.Page
{
    public string _mailChimpAPI = "2373668f5b54f904faefa75ae1e19117-us19";
    public string _mailChimpEndpoint = "https://us19.api.mailchimp.com/3.0/Lists/f1c4e1b184/members";
    public string _user = "GoldenComm";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        //Call method to sent to MailChimp
        SendToMailChimp();
    }

    public string SendToMailChimp()
    {
        string Result = string.Empty;
        string responseFromServer = string.Empty;
        string postdata = PrepareJson();

        try
        {
            string _auth = string.Format("{0}:{1}", _user, _mailChimpAPI);
            string _enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(_auth));

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_mailChimpEndpoint);

            request.Method = "POST";
            request.ContentType = "text/plain; charset=utf-8";
            request.Headers.Add("Authorization", "Basic " + _enc);
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
            request.Credentials = CredentialCache.DefaultCredentials;

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(postdata);
            }            

            //GET Responses
            HttpWebResponse responseStream = (HttpWebResponse)(request.GetResponse());
            Stream dataStream = responseStream.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            responseFromServer = reader.ReadToEnd();

            //dynamic parsedJson = JsonConvert.DeserializeObject(responseFromServer);
            //Result = JsonConvert.SerializeObject(parsedJson, Formatting.Indented);

            Response.Write(responseFromServer);

            reader.Close();
            dataStream.Close();
            responseStream.Close();

        }
        catch (Exception ex)
        {
            //write to event log
            Response.Write(ex.ToString());
        }

        return Result;
    }

    public string PrepareJson()
    {
        return "{  \"email_address\": \"tlai@goldencomm.com\",   \"status\": \"subscribed\",   \"merge_fields\": {  \"FNAME\": \"tamera\",  \"LNAME\": \"lai\"   }} ";
    }
}