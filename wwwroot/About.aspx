﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>

    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-md-2">
                <img class="img-circle" width="140" height="140" border="0" src="/img/Totoro.png"
                alt="" class="img-rounded img-responsive" />
            </div>
            <div class="col-sm-4 col-md-4">
                <blockquote>
                    <p>Mark Awa</p> <small><cite title="Source Title"> Long Beach, CA <i class="glyphicon glyphicon-map-marker"></i></cite></small>
                </blockquote>
                <p> <i class="glyphicon glyphicon-envelope"></i> markawa5[a]hotmail.com
                    <br /> <i class="glyphicon glyphicon-wrench"></i> Java
                    <br /> <i class="glyphicon glyphicon-user"></i> Developer</p>
            </div>
            <div class="col-sm-2 col-md-2">
                <img class="img-circle" width="140" height="140" border="0" src="/img/Totoro.png"
                alt="" class="img-rounded img-responsive" />
            </div>
            <div class="col-sm-2 col-md-4">
                <blockquote>
                    <p>Scott De Jesus</p> <small><cite title="Source Title">Long Beach, CA <i class="glyphicon glyphicon-map-marker"></i></cite></small>
                </blockquote>
                <p> <i class="glyphicon glyphicon-envelope"></i> sdjesus89[a]gmail.com
                    <br /> <i class="glyphicon glyphicon-wrench"></i> Java
                    <br /> <i class="glyphicon glyphicon-user"></i> Developer</p>
            </div>
        </div>
        <br />

        <div class="row">
            <div class="col-sm-2 col-md-2">
                <img class="img-circle" width="140" height="140" border="0" src="/img/Totoro.png"
                alt="" class="img-rounded img-responsive" />
            </div>
            <div class="col-sm-4 col-md-4">
                <blockquote>
                    <p>Osvaldo Garcia</p> <small><cite title="Source Title">Long Beach, CA <i class="glyphicon glyphicon-map-marker"></i></cite></small>
                </blockquote>
                <p> <i class="glyphicon glyphicon-envelope"></i> osvaldo.garcia.mail[a]gmail.com
                    <br /> <i class="glyphicon glyphicon-wrench"></i> Java
                    <br /> <i class="glyphicon glyphicon-user"></i> Developer</p>
            </div>
            <div class="col-sm-2 col-md-2">
                <img class="img-circle" width="140" height="140" border="0" src="/img/Totoro.png"
                alt="" class="img-rounded img-responsive" />
            </div>
            <div class="col-sm-2 col-md-4">
                <blockquote>
                    <p>Tamera Lai</p> <small><cite title="Source Title">Long Beach, CA <i class="glyphicon glyphicon-map-marker"></i></cite></small>
                </blockquote>
                <p> <i class="glyphicon glyphicon-envelope"></i> laithanhtam[a]gmail.com
                    <br /> <i class="glyphicon glyphicon-wrench"></i> Java
                    <br /> <i class="glyphicon glyphicon-user"></i> Developer</p>
            </div>
        </div>
        <br />

        <div class="row">
            <div class="col-sm-2 col-md-2">
                <img class="img-circle" width="140" height="140" border="0" src="/img/Totoro.png"
                alt="" class="img-rounded img-responsive" />
            </div>
            <div class="col-sm-4 col-md-4">
                <blockquote>
                    <p>Duy Mai</p> <small><cite title="Source Title">Long Beach, CA <i class="glyphicon glyphicon-map-marker"></i></cite></small>
                </blockquote>
                <p> <i class="glyphicon glyphicon-envelope"></i> duyqmai[a]gmail.com
                    <br /> <i class="glyphicon glyphicon-wrench"></i> Java
                    <br /> <i class="glyphicon glyphicon-user"></i> Developer</p>
            </div>
            <div class="col-sm-2 col-md-2">
                <img class="img-circle" width="140" height="140" border="0" src="/img/Totoro.png"
                alt="" class="img-rounded img-responsive" />
            </div>
            <div class="col-sm-2 col-md-4">
                <blockquote>
                    <p>Diego Tobar</p> <small><cite title="Source Title">Long Beach, CA <i class="glyphicon glyphicon-map-marker"></i></cite></small>
                </blockquote>
                <p> <i class="glyphicon glyphicon-envelope"></i> dl_tobar[a]yahoo.com
                    <br /> <i class="glyphicon glyphicon-wrench"></i> Java
                    <br /> <i class="glyphicon glyphicon-user"></i> Developer</p>
            </div>
        </div>
    </div>

</asp:Content>
