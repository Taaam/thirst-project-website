﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Thirst Project</h1>
        <p class="lead">Thirst Project on Android and iPhone</p>
        <p><a href="#" class="btn btn-primary btn-lg">learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>About Thirst Project</h2>
            <p>
               [Thirst Project]
            </p>
            <p>
                <a class="btn btn-default" href="#">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Social</h2>
            <p>
                User's Profile
            </p>
            <p>
                <a class="btn btn-default" href="#">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Events</h2>
            <p>
                Push Notification
            </p>
            <p>
                <a class="btn btn-default" href="#">Learn more &raquo;</a>
            </p>
        </div>
    </div>
</asp:Content>
